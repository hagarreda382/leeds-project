import { createApp } from "vue";
import App from "./App.vue";
import store from "./store"

import "./index.css";
import "flowbite";
import "flowbite-datepicker";
import "vue3-datepicker";
import "usemodal-vue3";


const app = createApp(App).use(store);

// app.use(Datepicker);
app.mount("#app");
